# frozen_string_literal: true

module Mirros
  module Source
    module MicrosoftTodo
      VERSION = '1.0.3'
    end
  end
end
