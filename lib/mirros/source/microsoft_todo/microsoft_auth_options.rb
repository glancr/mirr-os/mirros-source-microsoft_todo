# frozen_string_literal: true

module Mirros
  module Source
    module MicrosoftTodo
      # Class contains options, that specifies after creating instance of Hooks
      class MicrosoftAuthOptions
        class << self
          def client_id
            source_instance.configuration['applicationId']
          end

          def client_secret
            source_instance.configuration['clientSecret']
          end

          def instance_id
            source_instance.id
          end

          private

          def source_instance
            SourceInstance.find_by("source_id = 'microsoft_todo' and configuration->'$.graphTokenHash' is null")
          end
        end
      end
    end
  end
end
