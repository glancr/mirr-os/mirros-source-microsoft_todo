# frozen_string_literal: true

module Mirros
  module Source
    module MicrosoftTodo
      # Initializer create OAuth provider for Microsoft
      class Railtie < Rails::Railtie
        initializer 'mirros-source-microsoft_todo.configure_omniauth' do |app|
          app.middleware.use ActionDispatch::Session::CookieStore
          app.middleware.use OmniAuth::Builder do
            provider :microsoft_graph_auth, setup: (lambda do |env|
              env['omniauth.strategy'].options[:client_id] = MicrosoftAuthOptions.client_id
              env['omniauth.strategy'].options[:client_secret] = MicrosoftAuthOptions.client_secret
              env['omniauth.strategy'].options[:provider_ignores_state] = true
            end)
          end
        end
      end
    end
  end
end
