# frozen_string_literal: true

module Mirros
  module Source
    module MicrosoftTodo
      class Engine < ::Rails::Engine
        isolate_namespace Mirros::Source::MicrosoftTodo
        config.generators.api_only = true
      end
    end
  end
end
