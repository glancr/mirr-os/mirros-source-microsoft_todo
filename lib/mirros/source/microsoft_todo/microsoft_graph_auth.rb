# frozen_string_literal: true

require 'omniauth-oauth2'

module OmniAuth
  module Strategies
    # Implements an OmniAuth strategy to get a Microsoft Graph
    # compatible token from Azure AD
    class MicrosoftGraphAuth < OmniAuth::Strategies::OAuth2
      option :name, :microsoft_graph_auth

      DEFAULT_SCOPE = 'openid email profile user.read offline_access calendars.read tasks.read'

      # Configure the Microsoft identity platform endpoints
      option :client_options,
             site: 'https://login.microsoftonline.com',
             authorize_url: '/common/oauth2/v2.0/authorize',
             token_url: '/common/oauth2/v2.0/token'

      # Send the scope parameter during authorize
      option :authorize_options, [:scope]

      # Unique ID for the user is the id field
      uid { raw_info['id'] }

      # Get additional information after token is retrieved
      extra do
        {
          'raw_info' => raw_info
        }
      end

      def raw_info
        # Get user profile information from the /me endpoint
        @raw_info ||= access_token.get('https://graph.microsoft.com/v1.0/me').parsed
      end

      def authorize_params
        super.tap do |params|
          params['scope'.to_sym] = request.params['scope'] if request.params['scope']
          params[:scope] ||= DEFAULT_SCOPE
          # @see callback_url explanation.
          # FIXME: Assumes that production always runs with /api/ proxy. Investigate nginx proxy settings.
          params[:state] = full_host + (Rails.env.production? ? '/api' : '') + script_name + callback_path
        end
      end

      # Override callback URL
      # Hardcoded callback to work around redirect_uri restrictions on MS Azure Portal.
      # They only allow HTTP for localhost, so we need a static URL that forwards the
      # authorization code to our local device.
      # This is the officially recommended approach,
      # @see https://docs.microsoft.com/de-de/graph/auth-v2-user#authorization-request documentation.
      def callback_url
        'https://api.glancr.de/auth-proxy/microsoft-todo/callback/'
      end
    end
  end
end
