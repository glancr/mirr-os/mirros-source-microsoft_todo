# frozen_string_literal: true

require 'mirros/source/microsoft_todo/microsoft_graph_auth'
require 'mirros/source/microsoft_todo/version'
require 'mirros/source/microsoft_todo/engine'
require 'mirros/source/microsoft_todo/railtie' if defined?(Rails)
require 'mirros/source/microsoft_todo/microsoft_auth_options'
require 'httparty'

module Mirros
  module Source
    module MicrosoftTodo
      # Implements mirr.OS hooks for data source to-do.microsoft.com
      class Hooks
        REFRESH_INTERVAL = '5m'
        GRAPH_HOST = 'https://graph.microsoft.com'
        MISSING_TOKEN_TITLE = 'Sign In with Microsoft account, please'

        attr_reader :instance_id, :azure_app_id, :azure_app_secret

        # @return [String]
        def self.refresh_interval
          REFRESH_INTERVAL
        end

        # @param [Hash] configuration
        def initialize(instance_id, configuration)
          @instance_id = instance_id
          @azure_app_id = configuration['applicationId']
          @azure_app_secret = configuration['clientSecret']
        end

        def default_title
          if token_hash.present?
            microsoft_username
          else
            MISSING_TOKEN_TITLE
          end
        end

        def configuration_valid?
          azure_app_id.present? && azure_app_secret.present?
        rescue ArgumentError
          false
        end

        def list_sub_resources
          [[azure_app_id.to_s, 'Open tasks']]
        end

        def fetch_data(_group, sub_resources)
          # Fetch tasks from Microsoft
          response = make_api_call '/beta/me/outlook/tasks'
          raise response.parsed_response.to_s || "Request returned #{response.code}" unless response.code == 200

          tasks = response.parsed_response['value']
          # Create collection of reminder lists
          Mirros::Source::MicrosoftTodo::CollectionCreator.new(sub_resources, tasks).call
        end

        private

        def token_hash
          source_instance.configuration['graphTokenHash']&.symbolize_keys
        end

        def source_instance
          @source_instance ||= SourceInstance.find(instance_id)
        end

        def microsoft_username
          response = make_api_call '/v1.0/me'
          parsed_response = response.parsed_response
          "#{parsed_response['displayName']} (#{parsed_response['userPrincipalName']})"
        end

        def make_api_call(endpoint)
          HTTParty.get("#{GRAPH_HOST}#{endpoint}",
                       headers: auth_headers)
        end

        def auth_headers
          raise ArgumentError, 'Access token not set' if token_hash.blank?

          {
            Authorization: "Bearer #{access_token}"
          }
        end

        def access_token
          # Get the expiry time - 5 minutes
          expiry = Time.zone.at(token_hash[:expires_at] - 5.minutes)
          # Return the not expired token
          return token_hash[:token] if Time.zone.now <= expiry

          # Update the expired token and return it
          new_hash = refresh_tokens token_hash
          new_hash[:token]
        end

        def oauth_strategy
          OmniAuth::Strategies::MicrosoftGraphAuth.new(
            nil, azure_app_id, azure_app_secret
          )
        end

        def refresh_tokens(token_hash)
          token = OAuth2::AccessToken.new(
            oauth_strategy.client, token_hash[:token],
            refresh_token: token_hash[:refresh_token]
          )

          # Refresh the tokens
          new_tokens = token.refresh!.to_hash.slice(:access_token, :refresh_token, :expires_at)

          # Rename token key
          new_tokens[:token] = new_tokens.delete :access_token
          # Store the new hash
          source_instance.update!(
            configuration: source_instance.configuration.merge(graphTokenHash: new_tokens)
          )
          new_tokens
        end
      end
    end
  end
end
