# MicrosoftTodo
Short description and motivation.

## Usage
How to use my plugin.

## Installation
Add this line to your application's Gemfile:

```ruby
gem 'mirros-source-microsoft_todo'
```

And then execute:
```bash
$ bundle
```

Or install it yourself as:
```bash
$ gem install mirros-source-microsoft_todo
```

## Testing
To run the tests, simply execute the command:
```ruby
rspec
```

or the following command for clarity:

```ruby
rspec -fd
```

## Contributing
Contribution directions go here.

## License
The gem is available as open source under the terms of the [MIT License](https://opensource.org/licenses/MIT).
