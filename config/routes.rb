# frozen_string_literal: true

Rails.application.routes.draw do
  match '/auth/:provider/callback', to: 'mirros/source/microsoft_todo/auth#callback', via: %i[get post]
end
