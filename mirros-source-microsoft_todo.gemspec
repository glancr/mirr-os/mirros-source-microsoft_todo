# frozen_string_literal: true

require 'json'
$LOAD_PATH.push File.expand_path('lib', __dir__)

# Maintain your gem's version:
require 'mirros/source/microsoft_todo/version'

# Describe your gem and declare its dependencies:
Gem::Specification.new do |spec|
  spec.name        = 'mirros-source-microsoft_todo'
  spec.version     = Mirros::Source::MicrosoftTodo::VERSION
  spec.authors     = ['Alexey Zalyotov']
  spec.email       = ['alexey.zalyotov@rubyroidlabs.com']
  spec.homepage    = ''
  spec.summary     = 'Summary of MicrosoftTodo.'
  spec.description = 'Fetches tasks from your Microsoft account.'
  spec.license     = 'MIT'
  spec.metadata    = { 'json' =>
    {
      type: 'sources',
      title: {
        enGb: 'Microsoft To Do',
        deDe: 'Microsoft To Do',
        frFr: 'Microsoft To Do',
        esEs: 'Microsoft To Do',
        plPl: 'Microsoft To Do',
        koKr: 'Microsoft To Do'
      },
      description: {
        enGb: spec.description,
        deDe: 'Ruft Aufgaben von Deinem Microsoft-Konto ab.',
        frFr: 'Récupère les tâches de votre compte Microsoft.',
        esEs: 'Busca tareas de tu cuenta de Microsoft.',
        plPl: 'Pobiera zadania z Twojego konta Microsoft.',
        koKr: 'Microsoft 계정에서 작업을 가져옵니다.'
      },
      groups: [:reminder_list],
      compatibility: '0.0.0'
    }.to_json }

  spec.files = Dir['{app,config,db,lib}/**/*', 'MIT-LICENSE', 'Rakefile', 'README.md']

  spec.add_development_dependency 'rails', '~> 5.2'
  spec.add_development_dependency 'rubocop', '~> 0.81'
  spec.add_development_dependency 'rubocop-rails'
  spec.add_dependency 'omniauth', '~> 1.9'
  spec.add_dependency 'omniauth-oauth2', '~> 1.7.0'
end
