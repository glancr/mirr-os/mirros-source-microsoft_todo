# frozen_string_literal: true

class ReminderList < ApplicationRecord
  has_many :reminders

  attr_reader :tasks

  def update_or_insert_child(reminder_id, attributes)
    @tasks ||= {}
    @tasks[reminder_id] = attributes
  end
end
