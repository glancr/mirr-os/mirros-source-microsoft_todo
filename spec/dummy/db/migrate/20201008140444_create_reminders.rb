class CreateReminders < ActiveRecord::Migration[5.2]
  def change
    create_table :reminders do |t|
      t.string :uid
      t.json :attrs
      t.references :reminder_list, foreign_key: true

      t.timestamps
    end
  end
end
