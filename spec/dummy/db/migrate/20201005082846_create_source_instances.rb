class CreateSourceInstances < ActiveRecord::Migration[5.2]
  def change
    create_table :source_instances do |t|
      t.string :title
      t.string :source_id
      t.json :configuration
      t.json :options

      t.timestamps
    end
  end
end
