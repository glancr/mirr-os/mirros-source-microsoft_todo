# frozen_string_literal: true

require 'rails_helper'
require 'mirros/source/microsoft_todo/collection_creator'

RSpec.describe Mirros::Source::MicrosoftTodo::CollectionCreator, type: :model do
  let(:reminder_list) do
    create(:reminder_list) do |list|
      list.reminders.create(uid: '333')
      list.reminders.create(uid: '222')
    end
  end

  subject { Mirros::Source::MicrosoftTodo::CollectionCreator.new(reminders_id, tasks).call }

  describe '#call' do
    let(:tasks) { [{ 'id' => '333' }, { 'id' => '111' }] }

    context 'receive ids of created sub_resources' do
      let(:reminders_id) { [reminder_list.id] }

      it 'return reminders, presented in reminder list and in tasks with updated attributes' do
        expect(subject[0].tasks['333']).not_to be nil
      end

      it 'mark for destruction reminders, which are not presented in tasks' do
        expect(subject[0].reminders[1].marked_for_destruction?).to be true
      end

      it 'add new reminder from tasks' do
        expect(subject[0].tasks['111']).not_to be nil
      end
    end

    context 'receive id of unexisting sub_resource' do
      let(:reminders_id) { [reminder_list.id + 1] }

      it 'create new reminder list' do
        expect(subject[0].id).to eq(reminder_list.id + 1)
      end

      it 'add all reminders from tasks' do
        expect(subject[0].tasks.size).to eq(2)
        expect(subject[0].tasks['333']).not_to be nil
        expect(subject[0].tasks['111']).not_to be nil
      end
    end
  end
end
