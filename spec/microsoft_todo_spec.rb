# frozen_string_literal: true

require 'rails_helper'
require 'mirros/source/microsoft_todo'

RSpec.describe Mirros::Source::MicrosoftTodo::Hooks do
  let!(:source_instance) { create(:source_instance_without_token) }

  describe '.refresh_interval' do
    it 'returns 5m interval' do
      expect(Mirros::Source::MicrosoftTodo::Hooks.refresh_interval).to eql('5m')
    end
  end

  describe '#default_title' do
    subject { Mirros::Source::MicrosoftTodo::Hooks.new(source_instance.id, source_instance.configuration) }

    context 'when the token is still not received' do
      it 'is expected to eql "Sign In with Microsoft account, please"' do
        expect(subject.default_title).to eql(source_instance.title)
      end
    end

    context 'when the token received' do
      let(:source_instance) { create(:source_instance_with_token) }

      it 'is expected to eql "James Bond (agent007@gmail.com)"' do
        USER_NAME = 'James Bond (agent007@gmail.com)'
        allow(subject).to receive(:microsoft_username).and_return(USER_NAME)
        expect(subject.default_title).to eql(USER_NAME)
      end
    end
  end

  describe '#configuration_valid?' do
    subject { Mirros::Source::MicrosoftTodo::Hooks.new(source_instance.id, configuration).configuration_valid? }

    context 'when clientSecret is missing' do
      let(:configuration) { { 'applicationId': 'Application Id', 'clientSecret': '' } }
      it { is_expected.to be false }
    end

    context 'when applicationId is missing' do
      let(:configuration) { { 'applicationId': '', 'clientSecret': 'Client Secret' } }
      it { is_expected.to be false }
    end

    context 'when both attributes are missing' do
      let(:configuration) { { 'applicationId': '', 'clientSecret': '' } }
      it { is_expected.to be false }
    end

    context 'when both attributes are present' do
      let(:configuration) { source_instance.configuration }
      it { is_expected.to be true }
    end
  end

  describe '#list_sub_resources' do
    subject { Mirros::Source::MicrosoftTodo::Hooks.new(source_instance.id, source_instance.configuration).list_sub_resources }

    it 'returns an array with array contains 2 elements' do
      expect(subject[0]).to be_an_instance_of(Array)
      expect(subject[0].size).to eql(2)
      expect(subject[1]).to be_nil
    end
  end

  describe '#fetch_data' do
    subject { Mirros::Source::MicrosoftTodo::Hooks.new(source_instance.id, source_instance.configuration) }
    let(:response) do
      double(
        code: code,
        parsed_response: parsed_response
      )
    end
    before(:each) do
      allow(subject).to receive(:make_api_call).with('/beta/me/outlook/tasks').and_return(response)
    end

    context 'response code not 200' do
      let(:code) { Net::HTTPResponse::CODE_TO_OBJ['500'].new('1.1', 500, {}).code }
      let(:parsed_response) { 'Error' }
      it 'raise an error' do
        expect { subject.fetch_data('group', []) }.to raise_error('Error')
      end
    end

    context 'response code 200' do
      let(:code) { Net::HTTPResponse::CODE_TO_OBJ['200'].new('1.1', 200, {}).code }
      let(:first_reminder_list) do
        create(:reminder_list) do |list|
          list.reminders.create
        end
      end
      let(:second_reminder_list) do
        create(:reminder_list) do |list|
          list.reminders.create
        end
      end

      let(:parsed_response) { { 'value' => [{ uid: '333', attrs: { key: 'new reminder' } }] } }

      it 'returns array with just selected sub_resources' do
        expect(subject.fetch_data('group', [first_reminder_list.id])).to eql([first_reminder_list])
      end
    end
  end
end
