# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Mirros::Source::MicrosoftTodo::AuthController, type: :request do
  let!(:source_instance) { create(:source_instance_without_token) }

  before do
    Rails.application.env_config['omniauth.auth'] = OmniAuth.config.mock_auth[:microsoft_graph_auth]
  end

  describe '/auth/microsoft_graph_auth/callback' do
    it 'update configuration with graph token hash' do
      post '/auth/microsoft_graph_auth/callback'
      source_instance.reload
      expect(source_instance[:configuration]['graphTokenHash']).not_to be_nil
    end

    it 'redirect to mirrosSettingsLocation' do
      post '/auth/microsoft_graph_auth/callback'
      expect(response).to redirect_to(source_instance[:configuration]['mirrosSettingsLocation'])
    end
  end
end
