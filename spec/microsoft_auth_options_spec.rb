# frozen_string_literal: true

require 'rails_helper'
require 'mirros/source/microsoft_todo/microsoft_auth_options'

RSpec.describe Mirros::Source::MicrosoftTodo::MicrosoftAuthOptions do
  subject { Mirros::Source::MicrosoftTodo::MicrosoftAuthOptions }
  let!(:source_instance) { create(:source_instance_without_token) }
  let!(:source_instance_with_token) { create(:source_instance_with_token) }

  describe '.client_id' do
    it 'returns client_id of instance without token' do
      expect(subject.client_id).to eq(source_instance.configuration['applicationId'])
    end
  end

  describe '.client_secret' do
    it 'returns client_secret of instance without token' do
      expect(subject.client_secret).to eq(source_instance.configuration['clientSecret'])
    end
  end

  describe '.instance_id' do
    it 'returns instance_id of instance without token' do
      expect(subject.instance_id).to eq(source_instance.id)
    end
  end
end
