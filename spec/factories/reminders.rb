# frozen_string_literal: true

FactoryBot.define do
  factory :reminder do
    uid { "uid_#{rand(9999)}" }
    attrs { '' }
    reminder_list
  end
end
