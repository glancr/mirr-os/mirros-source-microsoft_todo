# frozen_string_literal: true

FactoryBot.define do
  factory :reminder_list, class: '::ReminderList' do
    name { "Name_#{rand(9999)}" }
  end
end
