# frozen_string_literal: true

FactoryBot.define do
  factory :source_instance do
    title { 'Sign In with Microsoft account, please' }
    source_id { 'microsoft_todo' }
    trait :without_token do
      configuration do
        {
          'clientSecret': 'clientSecretString',
          'applicationId': 'applicationIdString',
          'mirrosSettingsLocation': 'http://localhost:2020/#sources'
        }
      end
    end

    trait :with_token do
      configuration do
        {
          'clientSecret': 'clientSecretString.',
          'applicationId': 'applicationIdString',
          'mirrosSettingsLocation': 'http://localhost:2020/#sources',
          'graphTokenHash': { token: 'token_string' }
        }
      end
    end

    factory :source_instance_with_token, traits: [:with_token]
    factory :source_instance_without_token, traits: [:without_token]
  end
end
