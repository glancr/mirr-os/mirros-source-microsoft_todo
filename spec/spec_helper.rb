# frozen_string_literal: true

ENV['RAILS_ENV'] ||= 'test'

require 'bundler/setup'
Bundler.setup

# Dummy application
require 'dummy/config/environment'

require 'rspec/rails'

OmniAuth.config.test_mode = true
OmniAuth.config.mock_auth[:microsoft_graph_auth] = OmniAuth::AuthHash.new(
  {
    provider: :microsoft_graph_auth,
    uid: '0123456789012345',
    credentials: {
      expires: true,
      expires_at: 1601556375,
      refresh_token: 'refresh_token_string',
      token: 'token_string'
    }
  }
)

RSpec.configure do |config|
  config.expect_with :rspec do |expectations|
    expectations.include_chain_clauses_in_custom_matcher_descriptions = true
  end

  config.mock_with :rspec do |mocks|
    mocks.verify_partial_doubles = true
  end

  config.shared_context_metadata_behavior = :apply_to_host_groups

  config.include FactoryBot::Syntax::Methods
end
