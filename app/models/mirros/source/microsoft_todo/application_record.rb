# frozen_string_literal: true

module Mirros
  module Source
    module MicrosoftTodo
      class ApplicationRecord < ActiveRecord::Base
        self.abstract_class = true
      end
    end
  end
end
