# frozen_string_literal: true

module Mirros
  module Source
    module MicrosoftTodo
      class ReminderList < ::GroupSchemas::ReminderList
      end
    end
  end
end
