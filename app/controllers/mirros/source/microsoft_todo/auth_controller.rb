# frozen_string_literal: true

module Mirros
  module Source
    module MicrosoftTodo
      # Controller for Microsoft Authentication.
      class AuthController < ApplicationController
        def callback
          # Access the authentication hash for omniauth
          data = request.env['omniauth.auth']
          # Save the data in the configuration
          save_in_configuration(data)
          mirros_settings_host = source_instance.configuration['mirrosSettingsLocation']
          redirect_to mirros_settings_host
        end

        private

        def save_in_configuration(auth_hash)
          graph_token_hash = auth_hash.dig(:credentials)
          source_instance.update!(
            configuration: source_instance.configuration.merge(graphTokenHash: graph_token_hash)
          )
        end

        def source_instance
          @source_instance ||= SourceInstance.find(
            Mirros::Source::MicrosoftTodo::MicrosoftAuthOptions.instance_id
          )
        end
      end
    end
  end
end
