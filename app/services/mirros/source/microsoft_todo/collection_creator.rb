# frozen_string_literal: true

module Mirros
  module Source
    module MicrosoftTodo
      # Create collection on reminder task lists
      class CollectionCreator
        DEFAULT_TITLE = 'Open tasks'

        attr_reader :sub_resources, :tasks

        def initialize(sub_resources, tasks)
          @sub_resources = sub_resources
          @tasks = tasks
        end

        def call
          sub_resources.each_with_object([]) do |list_id, records|
            reminder_list = ReminderList.find_or_initialize_by(id: list_id)
            reminder_list.name = DEFAULT_TITLE

            reminder_list.reminders.each { |reminder| reminder.mark_for_destruction unless current_ids.include?(reminder.uid) }
            add_tasks(reminder_list)
            records << reminder_list
          end
        end

        private

        def current_ids
          @current_ids ||= tasks.map { |task| task['id'] }
        end

        def add_tasks(list)
          tasks.each do |task|
            due_date_time = task['dueDateTime'].present? ? task['dueDateTime']['dateTime'] : nil
            completed = task['status'] == 'completed'
            attrs = {
              uid: task['id'],
              creation_date: task['createdDateTime'],
              due_date: due_date_time,
              summary: task['subject'],
              completed: completed,
              assignee: nil # NOTE: Microsoft does not return a value and only Outlook tasks (deprecated) have this field
            }
            list.update_or_insert_child(task['id'], attrs)
          end
        end
      end
    end
  end
end
